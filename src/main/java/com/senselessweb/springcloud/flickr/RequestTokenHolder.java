package com.senselessweb.springcloud.flickr;

import java.util.concurrent.atomic.AtomicReference;

import org.scribe.model.Token;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class RequestTokenHolder {

	private final AtomicReference<Token> ref = new AtomicReference<>();

	public void set(Token token) {
		ref.set(token);
	}

	public Token get() {
		return ref.get();
	}
}
