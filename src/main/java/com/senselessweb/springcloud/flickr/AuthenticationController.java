package com.senselessweb.springcloud.flickr;

import java.util.concurrent.atomic.AtomicReference;

import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;

@Controller
public class AuthenticationController {
	
	private final AuthInterface auth;
	private final AtomicReference<Token> accessTokenHolder;
	private final RequestTokenHolder requestTokenHolder;
	
	public AuthenticationController(
			final AuthInterface auth,
			final AtomicReference<Token> accessTokenHolder,
			final RequestTokenHolder requestTokenHolder) {
		this.auth = auth;
		this.accessTokenHolder = accessTokenHolder;
		this.requestTokenHolder = requestTokenHolder;
	}

	@RequestMapping("/authenticate/")
	public RedirectView authenticate(
			final @Value("${CALLBACK_URL}") String callbackUrl) {
		requestTokenHolder.set(auth.getRequestToken(callbackUrl));
		final String redirect = auth.getAuthorizationUrl(requestTokenHolder.get(), Permission.READ);
		return new RedirectView(redirect);
	}

	@RequestMapping("/callback/")
	public ResponseEntity<String> callbackRoute(final @RequestParam("oauth_verifier") String verifier) {
		
		if (requestTokenHolder.get() == null) {
			return new ResponseEntity<String>("No request token found!", HttpStatus.NOT_ACCEPTABLE);
		}
		accessTokenHolder.set(auth.getAccessToken(requestTokenHolder.get(), new Verifier(verifier)));
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@RequestMapping("/authentication/")
	@ResponseBody
	public Token authentication() {
		return accessTokenHolder.get();
	}
}
