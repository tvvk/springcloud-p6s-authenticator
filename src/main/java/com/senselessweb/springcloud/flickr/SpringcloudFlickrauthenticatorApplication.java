package com.senselessweb.springcloud.flickr;

import java.util.concurrent.atomic.AtomicReference;

import org.scribe.model.Token;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.auth.AuthInterface;

@SpringBootApplication
public class SpringcloudFlickrauthenticatorApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SpringcloudFlickrauthenticatorApplication.class, args);
	}

	@Bean
	public AuthInterface auth(
			final @Value("${FLICKR_APIKEY}") String apiKey,
			final @Value("${FLICKR_APISECRET}") String apiSecret) {
		final Flickr flickr = new Flickr(apiKey, apiSecret, new REST());
		return flickr.getAuthInterface();
	}
	
	@Bean("accessTokenHolder")
	public AtomicReference<Token> accessTokenHolder() {
		return new AtomicReference<>();
	}
}
